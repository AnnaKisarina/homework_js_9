1. Опишіть, як можна створити новий HTML тег на сторінці.

document.createElement('tag');
document.body.innerHTML = '<tag>...</tag>';
document.body.insertAdjacentHTML('afterend', '<div>...</div>');


2. Опишіть, що означає перший параметр функції insertAdjacentHTML і опишіть можливі варіанти цього параметра.

Перший параметр це position, він визначає де саме буде додано елемент відносно елементу, викликаючого метод. Є такі варіанти:
- 'beforebegin': до всього element (до відкриваючого тегу);
- 'afterbegin': відразу після відкриваючого тегу element (перед першим нащадку).
- 'beforeend': відразу перед закрываючим тегом element (після останнього нащадку).
- 'afterend': після element (після закрываючого тегу).


3. Як можна видалити елемент зі сторінки?

node.remove();
document.body.innerHTML = '';