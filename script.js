let array = ["Kharkiv", 8, null, "Kyiv", ["Borispol", ['Lviv'], "Irpin"], "Odessa", "Lviv", "Dnieper"];
let sec = 3;

function createList(arr, parent) {
    let ul = document.createElement('ul');
    parent.append(ul);

    for (let arrElement of arr) {
        if (arrElement && typeof arrElement === 'object') {
            createList(arrElement, ul);
        } else {
            let li = document.createElement('li');
            li.innerText = arrElement;
            ul.append(li);
        }
    }
}

createList(array, document.body);

function timer() {
    if (sec < 1){
        document.body.innerHTML = '';
    } else {
        document.getElementById('timer').innerHTML = sec;
        setTimeout(timer, 1000);
        sec--
    } 
}

timer();